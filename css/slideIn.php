@keyframes slideInFromRandomXY {
	0%{
		transform: translate(<?php echo rand(-200,200);?>%,<?php echo rand(-200,200);?>%);
	}
}
@keyframes slideInFromTop {
	0%{
		transform: translate(0%,-100%);
	}
}
@keyframes slideInFromRight {
	0%{
		transform: translate(100%,0%);
	}
}
@keyframes slideInFromBottom {
	0%{
		transform: translate(0%,100%);
	}
}
@keyframes slideInFromLeft {
	0%{
		transform: translate(-100%,0%);
	}
}
.slideInFromRandomXY{
	animation: 2s ease-in-out 0s slideInFromRandomXY;
}
.slideInFromTop{
	animation: 2s ease-in-out 0s slideInFromTop;
}
.slideInFromRight{
	animation: 2s ease-in-out 0s slideInFromRight;
}
.slideInFromBottom{
	animation: 2s ease-in-out 0s slideInFromBottom;
}
.slideInFromLeft{
	animation: 2s ease-in-out 0s slideInFromLeft;
}
