<div id="forgot" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Mot de passe oublié</h3>
			</div>
			<div class="modal-body">
				<form class="form-horizontal col-xs-12" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
					<div class="form-group text-center">
						<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
							<input type="email" name="mail_for_password" class="form-control" placeholder="E-mail" required>
						</div>
					</div>
					<div class="form-group col-xs-12 text-center">
						<button type="submit" class="btn btn-primary">Envoyer</button>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>

