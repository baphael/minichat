<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
        <head>
                <title>LWS Minichat</title>
                <meta charset="utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="/js/loader.js"></script>
<?php
                if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
                        <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
<?php
                } else {
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
                        <link rel="stylesheet" href="css/paper.css">
<?php
                }
?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
		<link rel="icon" type="image/png" href="img/favicon.png" />
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<style>
			label > input{
				visibility: hidden;
				display: none;
			}
			label > input + img{
				cursor:pointer;
				border-radius:50%;
				border:2px solid transparent;
			}
			label > input:checked + img{
				border:2px solid #777;
				box-shadow: 0 0 5px #777;
			}
			label > input + span{
				position:absolute!important;
				top:-25px!important;
				right:-25px;
				cursor:pointer;
				padding:15px;
				font-size:200%;
				border-radius:50%;
				border:1px solid #333;
			}
			label > input:active + span{
				border:1px solid #777;
				box-shadow: 0 0 5px #777;
			}
		</style>
        </head>
        <body>
		<div class="loader"></div>
		<nav class="navbar navbar-inverse" style="margin-bottom:0px;">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
						<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
					<span class="navbar-brand"><i class="fa fa-comments-o" style="text-shadow: 0 0 5px #eee; font-size: 48px; color: #fff; height: 72px; width: 72px; line-height: 50%; text-align: center;"></i></span>
				</div>
				<div class="collapse navbar-collapse" id="navigation">
					<ul class="nav navbar-nav">
						<li><a href="/">Accueil</a></li>
						<li class="active"><a href="member.php">Espace membres</a></li>
						<li><a href="minichat.php">Salle de chat</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a data-toggle="modal" data-target="#logout" style="cursor:pointer;">Déconnexion <span class="glyphicon glyphicon-log-out"></span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="jumbotron text-center">
			<h1>LWS Minichat<br><small>Local Web Server</small></h1>
		</div>
		<div class="container">
<?php
			include 'logout_modal.php';
			if(isset($_SESSION['user_id']) and (!isset($_SESSION['free_user_nickname']))){
				if(isset($_GET['id'])){
					$query_user_data=mysqli_query($connexion,"select * from users where user_id=".$_GET['id']) or die ("Erreur SQL : ".$mysqli_error($connexion));
					if(mysqli_num_rows($query_user_data)>0){
						$user_data=mysqli_fetch_array($query_user_data);
						$nmsg=mysqli_fetch_array(mysqli_query($connexion,"select count(message_id) from posted_by where user_id=".$_GET['id'])) or die ("Erreur SQL : ".$mysqli_error($connexion));
						include 'searchbar.html';
						if(isset($_POST['like_user'])){
							$grab_alike=mysqli_fetch_array(mysqli_query($connexion,"select count(*) from user_likes where user_id_from=".$_SESSION['user_id']." and user_id_to=".$user_data['user_id']))[0];
							echo "<div class='clearfix'></div>";
							if($grab_alike==0){
								if(mysqli_query($connexion,"insert into user_likes values (".$_SESSION['user_id'].",".$_GET['id'].")")===TRUE){
?>
									<div class="alert alert-success alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Vous aimez le profil de <b><?php echo $user_data['user_nickname']; ?></b> !
									</div>
<?php
								} else {
?>
									<div class="alert alert-danger alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Vôtre vote n'a pas pu être pris en compte !<br>
										<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
									</div>
<?php
								}
							} else {
								if(mysqli_query($connexion,"delete from user_likes where user_id_from=".$_SESSION['user_id']." and user_id_to=".$user_data['user_id'])===TRUE){
?>
									<div class="alert alert-success alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Vous n'aimez plus le profil de <b><?php echo $user_data['user_nickname']; ?></b> !
									</div>
<?php
								} else {
?>
									<div class="alert alert-danger alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Vôtre vote n'a pas pu être pris en compte !<br>
										<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
									</div>
<?php
								}
							}
						}
?>
						<div class="well well-lg col-xs-12 col-md-offset-3 col-md-6" style="margin-top:50px;">
							<img class="img-circle" src="<?php echo $user_data['user_image']; ?>" width="96" style="position:absolute;top:-50px;left:50%;margin-left:-48px;">
							<h2 class="text-center"><?php echo ucfirst($user_data['user_firstname'])." ".ucfirst($user_data['user_lastname'])." <small>aka. ".$user_data['user_nickname']; ?></small></h2>
							<hr style="border: 1px solid #ddd;">
							<p class="lead"><span class="glyphicon glyphicon-user"></span> <?php echo ucfirst($user_data['user_sexe'])." - ".$user_data['user_age']." ans"; ?></p>
							<p class="lead"><span class="glyphicon glyphicon-pencil"></span> <?php echo $nmsg[0]; ?> posts</p>
							<p class="lead"><span class="glyphicon glyphicon-globe"></span> <?php echo $user_data['user_country']; ?></p>
							<p class="lead"><span class="glyphicon glyphicon-envelope"></span><a href='mailto:<?php echo $user_data['user_mail']; ?>'> <?php echo $user_data['user_mail']; ?></a></p>
							<p class="lead"><span class="glyphicon glyphicon-thumbs-up"></span> <?php $nlikes=mysqli_fetch_array(mysqli_query($connexion,"select count(*) from user_likes where user_id_to=".$_GET['id']))[0]; echo $nlikes; ?></p>
							<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $_GET['id']; ?>" style="float:right;">
								<label>
									<input type="submit" name="like_user" value="<?php echo $_GET['id']; ?>">
<?php
									$grab_alike=mysqli_fetch_array(mysqli_query($connexion,"select count(*) from user_likes where user_id_from=".$_SESSION['user_id']." and user_id_to=".$user_data['user_id']))[0];
									if($grab_alike==0){
?>
										<span class="glyphicon glyphicon-thumbs-up btn-primary"></span>
<?php
									} else {
?>
										<span class="glyphicon glyphicon-thumbs-down btn-danger"></span>
<?php
									}
?>
								</label>
							</form>
						</div>
<?php
					} else {
?>
						<div class="text-center alert alert-danger">
							<h2>Oups...</h2><h4>Aucun utilisateur ne correspond à l'ID '<?php echo Addslashes(htmlspecialchars($_GET['id'])); ?>'</h4>
							<a href="/">
								<button type="button" class="btn btn-default">Retour</button>
							</a>
						</div>
<?php
					}
				} else {
					include 'searchbar.html';
					echo "<div class='clearfix'></div>";
					if(isset($_POST['mod_member_profile'])){
						$grab=mysqli_fetch_array(mysqli_query($connexion,"select * from users where user_id=".$_SESSION['user_id']));
						if($_POST['user_nickname']!=$grab['user_nickname']){
							if(mysqli_num_rows(mysqli_query($connexion,"select * from users where user_nickname like '".Addslashes(htmlspecialchars($_POST['user_nickname']))."'"))==0){
								if(mysqli_query($connexion,"update users set user_nickname='".Addslashes(htmlspecialchars($_POST['user_nickname']))."' where user_id=".$_SESSION['user_id'])===TRUE){
									$_SESSION['user_nickname']=Addslashes(htmlspecialchars($_POST['user_nickname']));
?>
									<div class="alert alert-success alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Votre pseudo a été mis à jour avec succès, <b><?php echo Addslashes(htmlspecialchars($_POST['user_nickname'])); ?></b> !<br>
										<strong>Remarque : votre login est toujours '<?php echo $_SESSION['user_login']; ?>'.</strong>
									</div>
<?php
								} else {
?>
									<div class="alert alert-danger alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Une erreur est survenue lors de la mise à jour de votre pseudo !<br>
										<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
									</div>
<?php
								}
							} else {
?>
								<div class="alert alert-danger alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Le pseudo '<?php echo Addslashes(htmlspecialchars($_POST['user_nickname'])); ?>' est déjà pris !
								</div>
<?php
							}
						}
						if(($_POST['user_sexe']!=$grab['user_sexe']) and ($_POST['user_sexe']!="") and (strtolower($_POST['user_sexe'])!="sexe")){
							if(mysqli_query($connexion,"update users set user_sexe='".$_POST['user_sexe']."' where user_id=".$_SESSION['user_id'])===TRUE){
								$_SESSION['user_sexe']=$_POST['user_sexe'];
?>
								<div class="alert alert-success alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Votre sexe a été mis à jour avec succès !<br>
									<strong>Attention, changer de sexe peut avoir de lourdes conséquences sur la forme de votre caleçon !</strong>
								</div>
<?php
							} else {
?>
								<div class="alert alert-danger alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									LWS a apparemment refusé votre changement de sexe, désolé !<br>Si cette erreur persiste, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.
									<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
								</div>
<?php
							}
						}
						if(($_POST['user_age']!=$grab['user_age']) and ($_POST['user_age']!="")){
							if(mysqli_query($connexion,"update users set user_age=".$_POST['user_age']." where user_id=".$_SESSION['user_id'])===TRUE){
								$_SESSION['user_age']=$_POST['user_age'];
?>
								<div class="alert alert-success alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Votre âge a été mis à jour avec succès !
								</div>
<?php
							} else {
?>
								<div class="alert alert-danger alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Une erreur est survenue lors de la mise à jour de votre âge !<br>
									<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
								</div>
<?php
							}
						}
						if(($_POST['user_country']!=$grab['user_country']) and ($_POST['user_country']!="") and (strtolower($_POST['user_country'])!="pays")){
							if(mysqli_query($connexion,"update users set user_country='".$_POST['user_country']."' where user_id=".$_SESSION['user_id'])===TRUE){
								$_SESSION['user_country']=$_POST['user_country'];
?>
								<div class="alert alert-success alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Votre pays a été mis à jour avec succès !
								</div>
<?php
							} else {
?>
								<div class="alert alert-danger alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Une erreur est survenue lors de la mise à jour de votre pays !<br>
									<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
								</div>
<?php
							}
						}
						if($_POST['user_image']!=$grab['user_image']){
							if($_POST['user_image']!=""){
								if(file_exists($_POST['user_image'])){
									if(mysqli_query($connexion,"update users set user_image='".$_POST['user_image']."' where user_id=".$_SESSION['user_id'])===TRUE){
										$_SESSION['user_image']=$_POST['user_image'];
?>
										<div class="alert alert-success text-center" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											Votre avatar a été mis à jour avec succès !
										</div>
<?php
									} else {
?>
										<div class="alert alert-danger text-center" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											Une erreur set survenue lors de la mise à jour de votre avatar !<br>
											<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
										</div>
<?php
									}
								} else {
?>
									<div class="alert alert-danger text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										L'avatar que vous avez sélectionné n'est plus disponible !<br>
										Si cette erreur persiste, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.
									</div>
<?php
								}
							} else {
?>
								<div class="alert alert-danger text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									P'tit malin, t'as pas le droit de sélectionner un avatar vide !
								</div>
<?php
							}
						}
						if(($_POST['user_theme']!=$grab['user_theme']) and (($_POST['user_theme']=="paper") or ($_POST['user_theme']=="darkly"))){
							if(mysqli_query($connexion,"update users set user_theme='".$_POST['user_theme']."' where user_id=".$_SESSION['user_id'])===TRUE){
								$_SESSION['user_theme']=$_POST['user_theme'];
?>
								<div class="alert alert-success text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Le thème a été mis à jour avec succès !<br><a href="<?php echo $_SERVER['PHP_SELF']; ?>"><button type="button" class="btn btn-default">Recharger la page</button></a>
								</div>
<?php
							} else {
?>
								<div class="alert alert-success text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Désolé, le thème que vous avez sélectionné n'est pas applicable pour l'instant !<br>
									Si le problème persiste, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.<br>
									<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
								</div>
<?php
							}
						}
					} elseif(isset($_POST['mod_member_password'])){
						$grab=mysqli_fetch_array(mysqli_query($connexion,"select * from users where user_id=".$_SESSION['user_id']));
						$hrh_old_password=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['old_password']))."')))"))[0];
						$hrh_new_password=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['new_password']))."')))"))[0];
						$hrh_new_password_confirm=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['new_password_confirm']))."')))"))[0];
						if($hrh_old_password==$grab['user_password']){
							if($hrh_new_password!=$grab['user_password']){
								if($hrh_new_password==$hrh_new_password_confirm){
									if($_POST['new_password']!=""){
										if(mysqli_query($connexion,"update users set user_password='".$hrh_new_password."' where user_id=".$_SESSION['user_id'])===TRUE){
?>
											<div class="alert alert-success alert-dismissible text-center" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												Votre mot de passe a été mis à jour avec succès !
											</div>
<?php
										} else {
?>
											<div class="alert alert-danger alert-dismissible text-center" role="alert">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												Une erreur est survenue lors de la mise à jour de votre mot de passe !<br>
												<strong>Erreur : <?php echo mysqli_error($connexion); ?></strong>
											</div>
<?php
										}
									} else {
?>
										<div class="alert alert-danger alert-dismissible text-center" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											P'tit malin... T'as pas le droit de mettre un mot de passe vide !!
										</div>
<?php
									}
								} else {
?>
									<div class="alert alert-danger alert-dismissible text-center" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										Les mots de passe que vous avez saisi ne correspondent pas !
									</div>
<?php
								}
							} else {
?>
								<div class="alert alert-info alert-dismissible text-center" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									Vous avez saisi le même mot de passe que le précédent...
								</div>
<?php
							}
						} else {
?>
							<div class="alert alert-danger alert-dismissible text-center" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								Le mot de passe que vous avez saisi ne correspond pas à votre mot de passe actuel !
							</div>
<?php
						}
					}
?>
					<div class="well well-lg col-xs-12 col-md-offset-3 col-md-6" style="padding:0px;">
						<ul class="nav nav-tabs">
							<li class="active col-xs-6 text-center" style="padding:0px;"><a href="#mod_member_profile" data-toggle="tab"><h4>Informations personnelles</h4></a></li>
							<li class="col-xs-6 text-center" style="padding:0px;"><a href="#mod_member_password" data-toggle="tab"><h4>Mot de passe</h4></a></li>
						</ul>
						<div class="tab-content" style="margin:24px;">
							<div class="tab-pane active" id="mod_member_profile">
<!--
								<h2 class="text-center"><?php // echo ucfirst($_SESSION['user_firstname'])." ".$_SESSION['user_lastname']." <small>aka. ".$_SESSION['user_nickname'];?></small></h2>
								<hr style="border: 1px solid #ddd;">
-->
								<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
									<div style="margin: auto;" class="text-center form-group col-xs-12" id="avatar">
<?php
										$dir="user_image/*.png";
										$files = glob($dir,GLOB_BRACE);
										foreach($files as $image){
											$f= str_replace($dir,'',$image);
											echo "<label><input type='radio' name='user_image' value='".$f."'".(($_SESSION['user_image']==$f)?" checked ":" ")."><img src='".$f."' height='64' width='64'></label>";
										}
?>
									</div>
									<div class="form-group col-xs-12">
										<label class="col-xs-4 control-label text-right">Pseudonyme :</label>
										<div class="col-xs-8 text-left">
											<input class="form-control" size="12" value='<?php echo $_SESSION['user_nickname']; ?>' type="text" maxlength="31" name="user_nickname">
										</div>
									</div>
									<div class="form-group col-xs-12">
										<label class="col-xs-4 text-right control-label">Sexe :</label>
										<div class="col-xs-8 text-left">
											<select name="user_sexe" class="selectpicker form-control" data-width="100%" required>
												<option value="homme"<?php if($_SESSION['user_sexe']=="homme"){?> selected<?php } ?>>Homme</option>
												<option value="femme"<?php if($_SESSION['user_sexe']=="femme"){?> selected<?php } ?>>Femme</option>
												<option value="autre"<?php if($_SESSION['user_sexe']=="autre"){?> selected<?php } ?>>Autre</option>
											</select>
										</div>
									</div>
									<div class="col-xs-12 form-group">
										<label class="control-label text-right col-xs-4">Âge :</label>
										<div class="text-right col-xs-8">
											<input class="form-control" type="number" min="3" max="99" value='<?php echo $_SESSION['user_age'];?>' name="user_age">
										</div>
									</div>
									<div class="form-group col-xs-12">
										<label class="control-label text-right col-xs-4">Pays <small>(<?php echo $_SESSION['user_country']; ?>)</small> :</label>
										<div class="col-xs-8 text-left">
											<?php include 'country.html'; ?>
										</div>
									</div>
									<div class="form-group col-xs-12">
										<label class="control-label text-right col-xs-6 col-md-4">Thème :</label>
										<div class="col-xs-6 col-md-8 text-left">
											<label><input type="radio" name="user_theme" value="paper" <?php if($_SESSION['user_theme']=="paper"){ echo "checked"; } ?>><img class="img-rounded" style="background: #eee; " width="32" height="32"></label>
											<label><input type="radio" name="user_theme" value="darkly" <?php if($_SESSION['user_theme']=="darkly"){ echo "checked"; } ?>><img class="img-rounded" style="background: #222;" width="32" height="32"></label>
										</div>
									</div>
									<input type="hidden" name="mod_member_profile">
									<div class="form-group text-center col-xs-12">
										<button type="submit" class="btn btn-md btn-primary">Enregistrer</button>
									</div>
								</form>
							</div>
							<div class="tab-pane" id="mod_member_password">
								<form class="form-horizontal col-xs-12" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
									<div class="form-group text-center">
										<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
											<input type="password" name="old_password" placeholder="Mot de passe actuel" class="form-control" required>
										</div>
									</div>
									<div class="form-group text-center">
										<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
											<input type="password" name="new_password" placeholder="Nouveau mot de passe" class="form-control" required>
										</div>
									</div>
									<div class="form-group text-center">
										<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
											<input type="password" name="new_password_confirm" placeholder="Nouveau mot de passe (confirm.)" class="form-control" required>
										</div>
									</div>
									<input type="hidden" name="mod_member_password">
									<div class="form-group text-center col-xs-12">
										<button type="submit" class="btn btn-md btn-primary">Modifier</button>
									</div>
								</form>
							</div>
						</div>
					</div>
<?php
				}
			} else {
?>
				<div class="alert alert-danger text-center">
					<h2>Oups...</h2><h4>Vous devez vous connecter pour accéder à cet espace !</h4>
					<a href="/">
						<button type="button" class="btn btn-default" autofocus>Retour</button>
					</a>
				</div>
<?php
				// header('Location: index.php');
	 		}
?>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
        </body>
</html>
