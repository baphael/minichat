<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
	<head>
		<title>LWS Minichat</title>
		<meta charset="utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="/js/loader.js"></script>
<?php
                if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
                        <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
<?php
                } else {
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
                        <link rel="stylesheet" href="css/paper.css">
<?php
                }
?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	</head>
	<body>
		<div class="loader"></div>
                <div class="jumbotron text-center">
                        <h1>LWS Minichat<br><small>Local Web Server</small></h1>
                </div>
		<div class="container-fluid">
			<div class="container" style="padding: 5%;">
<?php
			if(isset($_SESSION['user_id'])){
				if(!isset($_SESSION['free_user_nickname'])){
					#Destruction de la session précédente
					$_SESSION = array();
					session_destroy();
					unset($_SESSION);
					if(!isset($_SESSION['user_id'])){
?>
						<div class="alert alert-info text-center">
							<h2>Vous avez été déconnecté</h2><br>
							<a href="/">
								<button type="button" class="btn btn-default" autofocus>Valider</button>
							</a>
						</div>
<?php
					} else {
?>
						<div class="alert alert-danger text-center">
						<h2>Une erreur s'est produite lors de votre déconnexion !</h2><br><h4>Veuillez réessayer.</h4><br>
							<a href="/">
								<button type="button" class="btn btn-default" autofocus>Retour</button>
							</a>
						</div>
<?php
					}
				} else {
					if(mysqli_query($connexion,"delete from posted_by where user_id=".$_SESSION['user_id'])===TRUE){
						if(mysqli_query($connexion,"delete from users where user_id=".$_SESSION['user_id'])===TRUE){
							$_SESSION=array();
							session_destroy();
							unset($_SESSION);
							if(!isset($_SESSION['user_id'])){
?>
								<div class="alert alert-info text-center">
									<h2>Vous avez quitté la session libre.</h2><br>
									<a href="/">
										<button type="button" class="btn btn-default" autofocus>Valider</button>
									</a>
								</div>
<?php
							} else {
								$error="Damn, erreur PHP #1 (SESSION is not dead !)";
							}
						} else {
							$error="Damn, erreur SQL #2 (".mysqli_error($connexion).")";
						}
					} else {
						$error="Damn, erreur SQL #1 (".mysqli_error($connexion).")";
					}
					if(isset($error)){
?>
						<div class="alert alert-danger text-center">
							<h3><?php echo $error; ?></h3>
							<a href="/">
								<button type="button" class="btn btn-default" autofocus>Retour</button>
							</a>
						</div>
<?php
					}
				}
			} else {
?>
					<div class="alert alert-danger text-center">
						<h2>Oups...</h2><br><h4>Vous devez vous connecter pour accéder à cette page !</h4><br>
						<a href="/">
							<button type="button" class="btn btn-default" autofocus>Retour</button>
						</a>
					</div>
<?php
			}
?>
			</div>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
	</body>
</html>
