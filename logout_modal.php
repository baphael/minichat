<?php
if(isset($_SESSION['free_user_nickname'])){
	$ask="Êtes-vous sûr(e) de vouloir quitter la session libre, ".$_SESSION['free_user_nickname']." ?";
	$explain="<small>En quittant la session libre, vous rendrez le pseudo '".$_SESSION['free_user_nickname']."' utilisable par d'autres.</small>";
} else {
	$ask="Êtes-vous sûr(e) de vouloir vous déconnecter, ".ucfirst($_SESSION['user_firstname'])." ?";
}
?>
<!-- Logout Modal -->
<div class="modal fade" id="logout" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title text-center">Déconnexion</h3>
			</div>
			<div class="modal-body">
				<form method="post" action="logout.php">
					<h4 class="text-center"><?php echo $ask; if(isset($_SESSION['free_user_nickname'])){ echo "<br><br>".$explain; } ?></h4>
					<div class="row form-group"><br>
						<div class="col-xs-6 text-right">
							<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Annuler</button>
						</div>
						<div class="col-xs-6 text-left">
							<button type="submit" class="btn btn-primary btn-lg">Confirmer</button><br>
						</div>
					</div>
					<input type="hidden" name="login-form">
				</form>
			</div>
		</div>
	</div>
</div>

