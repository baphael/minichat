<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
	<head>
		<title>LWS Minichat</title>
		<meta charset="utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="/js/loader.js"></script>
<?php
                if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
                        <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
<?php
                } else {
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
                        <link rel="stylesheet" href="css/paper.css">
<?php
                }
?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
		<link rel="icon" type="image/png" href="img/favicon.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	</head>
	<body>
		<div class="loader"></div>
		<div class="jumbotron text-center">
			<h1>LWS Minichat<br><small>Local Web Server</small></h1>
		</div>
		<div class="container">
<?php
			if(isset($_GET['uid'])){
				if(isset($_GET['tid'])){
					if(isset($_GET['mid'])){
						if(isset($_GET['req'])){
							$greq=mysqli_fetch_array(mysqli_query($connexion,"select unhex(reverse(unhex('".$_GET['req']."')))"))[0];
							$guid=mysqli_fetch_array(mysqli_query($connexion,"select unhex(reverse(unhex('".$_GET['uid']."')))"))[0];
							$gtid=mysqli_fetch_array(mysqli_query($connexion,"select unhex(reverse(unhex('".$_GET['tid']."')))"))[0];
							$gmid=mysqli_fetch_array(mysqli_query($connexion,"select unhex(reverse(unhex('".$_GET['mid']."')))"))[0];
							$query_user_by_get=mysqli_query($connexion,"select * from users where user_id=".$guid." and user_logged_since='".$gtid."' and user_mail like '".$gmid."'") or die ("Erreur SQL : ".mysqli_error($connexion));
							if(mysqli_num_rows($query_user_by_get)>0){
								$get_user=mysqli_fetch_array($query_user_by_get);
								if($greq=="check"){
									if(!$get_user['user_checked']){
										if(mysqli_query($connexion,"update users set user_checked=TRUE where user_id=".$guid)===TRUE){
?>
											<div class="alert alert-success text-center">
												<h2>Bravo !</h2><h4>Votre compte a été validé (<?php echo $gmid; ?>).<br>Vous pouvez maintenant vous connecter en tant que <b><?php echo $get_user['user_login']; ?></b>.</h4>
												<a href="/"><button type="button" class="btn btn-default" autofocus>Valider</button></a>
											</div>
<?php
										} else {
?>
											<div class="alert alert-danger text-center">
												<h2>Oups...</h2><h4>Erreur SQL ! (<?php echo mysqli_error($connexion); ?>)</h4>
												<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
											</div>
<?php
										}
									} else {
?>
										<div class="alert alert-info text-center">
											<h2>Hmm...</h2><h4>Votre compte a déjà été validé, <b><?php echo $get_user['user_nickname']; ?></b> !</h4>
											<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
										</div>
<?php
									}
								} elseif($greq=="password"){
?>
									<div class="well well-lg col-xs-12 col-md-offset-3 col-md-6">
										<h3 class="text-center">Modification du mot de passe</h3>
										<?php $ppreq=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('password_post')))"))[0]; ?>
										<form action='<?php echo $_SERVER['PHP_SELF']."?uid=".$_GET['uid']."&tid=".$_GET['tid']."&mid=".$_GET['mid']."&req=".$ppreq; ?>' method="post" class="form-horizontal col-xs-12">
											<div class="form-group text-center">
												<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
													<input type="password" name="new_password" placeholder="Nouveau mot de passe" class="form-control" autofocus required>
												</div>
											</div>
											<div class="form-group text-center">
												<div class="col-xs-12 col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
													<input type="password" name="new_password_confirm" placeholder="Nouveau mot de passe (confirm.)" class="form-control" required>
												</div>
											</div>
											<div class="form-group text-center col-xs-12">
												<button type="submit" class="btn btn-primary">Enregistrer</button>
											</div>
										</form>
									</div>
<?php
								} elseif($greq=="password_post"){
									if((isset($_POST['new_password'])) and (!empty($_POST['new_password']))){
										if(Addslashes(htmlspecialchars($_POST['new_password']))==Addslashes(htmlspecialchars($_POST['new_password_confirm']))){
											if(mysqli_query($connexion,"update users set user_password=HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['new_password']))."'))) where user_id=".$guid)===TRUE){
?>
												<div class="alert alert-success text-center">
													<h2>Bravo !</h2><h4>Votre mot de passe a été mis à jour, <b><?php echo $get_user['user_nickname']; ?></b>.</h4>
													<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
												</div>
<?php
											} else {
?>
												<div class="alert alert-danger text-center">
													<h2>Oups...</h2><h4>Une erreur s'est produite lors de la mise à jour de votre mot de passe, <b><?php echo $get_user['user_nickname']; ?></b> !</h4>
													<strong><?php echo "Erreur : ".mysqli_error($connexion); ?></strong>
													<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
												</div>
<?php
											}
										} else {
?>
											<div class="alert alert-danger text-center">
												<h2>Hmm...</h2><h4>Les mots de passe que vous avez saisi ne correspondent pas !</h4>
												<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
											</div>
<?php
										}
									} else {
?>
										<div class="alert alert-danger text-center">
											<h2>Hmm...</h2><h4>Vous ne pouvez pas saisir un mot de passe vide !</h4>
											<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
										</div>

<?php									}
								} else {
?>
									<div class="alert alert-danger text-center">
										<h2>Oups...</h2><h4>Requête inconnue ! (<?php echo $_GET['req']; ?>)</h4>
										<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
									</div>
<?php
								}
							} else {
?>
									<div class="alert alert-danger">
										<h2>Oups...</h2><h4>Le compte correspondant à l'URL '<?php echo "http:\/\/".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>' n'existe pas !<br>Si vous avez reçu ce lien par mail, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.</h4>
										<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
									</div>
<?php
							}
						} else {
?>
							<div class="alert alert-danger text-center">
								<h2>Oups...</h2><h4>Le type de requête (REQ) n'est pas spécifié dans l'URL !<br>Si vous avez reçu ce lien par mail, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.</h4>
								<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
							</div>
<?php
						}
					} else {
?>
						<div class="alert alert-danger text-center">
							<h2>Oups...</h2><h4>Il manque l'identificateur de mail (MID) dans l'URL !<br>Si vous avez reçu ce lien par mail, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.</h4>
							<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
						</div>
<?php
					}
				} else {
?>
					<div class="alert alert-danger text-center">
						<h2>Oups...</h2><h4>Il manque l'identificateur de temps (TID) dans l'URL !<br>Si vous avez reçu ce lien par mail, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.</h4>
						<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
					</div>
<?php
				}
			} else {
?>
				<div class="alert alert-danger text-center">
					<h2>Oups...</h2><h4>Il manque l'identificateur d'utilisateur (UID) dans l'URL !<br>Si vous avez reçu ce lien par mail, veuillez contacter <a href="mailto:raph.berteaud@gmail.com">l'administrateur</a>.</h4>
					<a href="/"><button type="button" class="btn btn-default" autofocus>Retour</button></a>
				</div>
<?php
			}
?>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</body>
</html>
