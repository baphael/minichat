<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
<head>
  <title>LWS Minichat</title>
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="/js/loader.js"></script>
  <?php
  if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
    ?>
    <link rel="stylesheet" href="http://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
    <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
    <?php
  } else {
    ?>
    <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
    <link rel="stylesheet" href="css/paper.css">
    <?php
  }
  ?>
  <link rel='stylesheet' type='text/css' href='css/slideIn.php' />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
  <link rel="icon" type="image/png" href="img/favicon.png" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
  <?php if(($_SERVER['HTTP_REFERER']!="http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']) and ($_SERVER['HTTP_REFERER']!="http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'])){ ?><div class="loader"></div><?php } ?>
    <?php
    if(isset($_SESSION['user_id'])){
      if((isset($_POST['chatroom_name'])) and (!empty($_POST['chatroom_name']))){
        $_SESSION['chatroom_name']=Addslashes(htmlspecialchars($_POST['chatroom_name']));
      } else {
        if((!isset($_SESSION['chatroom_name'])) or (empty($_SESSION['chatroom_name']))){
          header("Location: index.php");
          die();
        }
      }
      if((mysqli_num_rows(mysqli_query($connexion,"select * from chatroom where chatroom_name='".$_SESSION['chatroom_name']."'")))==0){
        $max_cid=mysqli_fetch_array(mysqli_query($connexion,"select max(chatroom_id) from chatroom"));
        if((isset($_POST['chatroom_is_private'])) and ($_POST['chatroom_is_private']=="true")){
          if(mysqli_query($connexion,"insert into chatroom values (".($max_cid[0]+1).",'".$_SESSION['chatroom_name']."',TRUE,HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['chatroom_password']))."'))))")!==TRUE){
            include 'minichat_top.php';
            ?>
            <div class="container">
              <div class="alert alert-danger text-center">
                <h2>Oups...</h2><h4>Erreur SQL #1 <small>(<?php echo mysqli_error($connexion); ?>)</small></h4>
                <a href="/">
                  <button type="button" class="btn btn-default" autofocus>Retour</button>
                </a>
              </div>
            </div>
          </body>
          </html>
          <?php
          die();
        } else {
          $_SESSION['chatroom_password']=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['chatroom_password']))."')))"))[0];
          $_SESSION['chatroom_id']=$max_cid[0]+1;
        }
      } else {
        if(mysqli_query($connexion,"insert into chatroom values (".($max_cid[0]+1).",'".$_SESSION['chatroom_name']."',FALSE,'')")!==TRUE){
          include 'minichat_top.php';
          ?>
          <div class="container">
            <div class="alert alert-danger text-center">
              <h2>Oups...</h2><h4>Erreur SQL #2 <small>(<?php echo mysqli_error($connexion); ?>)</small></h4>
              <a href="/">
                <button type="button" class="btn btn-default" autofocus>Retour</button>
              </a>
            </div>
          </div>
        </body>
        </html>
        <?php
        die();
      } else {
        $_SESSION['chatroom_id']=$max_cid[0]+1;
      }
    }
  } else {
    $cdata=mysqli_fetch_array(mysqli_query($connexion,"select * from chatroom where chatroom_name='".$_SESSION['chatroom_name']."'"));
    if($cdata['chatroom_is_private']){
      if((!isset($_SESSION['chatroom_password'])) or ($_SESSION['chatroom_password']!=$cdata['chatroom_password'])){
        if(!isset($_SESSION['free_user_nickname'])){
          if(!isset($_POST['chatroom_password']) or (empty($_POST['chatroom_password']))){
            if(!isset($_POST['ask_chatroom_password'])){
              include 'minichat_top.php';
              ?>
              <div class="container">
                <div class="col-xs-12 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4">
                  <div class="panel panel-danger">
                    <div class="panel-heading">
                      <h4>La salle '<?php echo $cdata['chatroom_name']; ?>' est privée !</h4>
                    </div>
                    <div class="panel-body text-center">
                      <form method="post" action='<?php echo $_SERVER['PHP_SELF']; ?>'>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                          <input type="password" name="ask_chatroom_password" maxlength="63" class="input-lg form-control" placeholder="Mot de passe" autofocus required>
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-danger" style="padding:15px;"><i class="glyphicon glyphicon-arrow-right"></i></button>
                          </span>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </body>
            </html>
            <?php
            die();
          } else {
            $hrh_post_ask_chatroom_password=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['ask_chatroom_password']))."')))"))[0];
            if($hrh_post_ask_chatroom_password!=$cdata['chatroom_password']){
              include 'minichat_top.php';
              ?>
              <div class="container">
                <div class="alert alert-danger text-center">
                  <h2>Oups...</h2><h4>Le mot de passe que vous avez saisi n'est pas valable pour cette salle !</h4>
                  <a href="/">
                    <button type="button" class="btn btn-default" autofocus>Retour</button>
                  </a>
                </div>
              </div>
            </body>
            </html>
            <?php
            die();
          } else {
            $_SESSION['chatroom_password']=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['ask_chatroom_password']))."')))"))[0];
          }
        }
      } else {
        $hrh_post_chatroom_password=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['chatroom_password']))."')))"))[0];
        if($hrh_post_chatroom_password!=$cdata['chatroom_password']){
          include 'minichat_top.php';
          ?>
          <div class="container">
            <div class="alert alert-danger text-center">
              <h2>Oups...</h2><h4>Le mot de passe que vous avez saisi n'est pas valable pour cette salle !</h4>
              <a href="/">
                <button type="button" class="btn btn-default" autofocus>Retour</button>
              </a>
            </div>
          </div>
        </body>
        </html>
        <?php
        die();
      } else {
        $_SESSION['chatroom_password']=mysqli_fetch_array(mysqli_query($connexion,"select HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['chatroom_password']))."')))"))[0];
      }
    }
  } else {
    include 'minichat_top.php';
    ?>
    <div class="container">
      <div class="alert alert-danger text-center">
        <h2>Oups...</h2><h4>Vous devez vous connecter pour accéder à une salle de chat privée !</h4>
        <a href="/">
          <button type="button" class="btn btn-default" autofocus>Retour</button>
        </a>
      </div>
    </div>
  </body>
  </html>
  <?php
  die();
}
}
}
$_SESSION['chatroom_id']=$cdata[0];
}
include 'minichat_top.php';
?>
<div class="container">
  <div class="panel panel-default slideInFromBottom">
    <div class="panel-heading text-center no-sm">
      <h3 class="no-sm no-sm-pad">Salle : <?php echo $_SESSION['chatroom_name'];?></h3>
    </div>
    <div class="panel-body" id="scroll" style="height:40vh;overflow-y:scroll;">
      <div id="messages">
        <?php
        include 'messages.php';
        ?>
      </div>
    </div>
    <div class="panel-footer">
      <form method="post" action="minichat_post.php">
        <div class="input-group col-xs-12">
          <input name="message_content" type="text" maxlength="500" placeholder="Message" class="form-control input-lg" autocomplete="off" autofocus required>
          <span class="input-group-btn">
            <button type="submit" class="btn btn-primary" style="padding:15px;">
              <i class="glyphicon glyphicon-comment"></i>
            </button>
          </span>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
} else {
  header("Location: index.php");
}
?>
<script src="/js/ajax-reload.js"></script>
<script src="/js/scrolldown.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
</body>
</html>
