<div class="modal fade" id="login" role="dialog">
        <div class="modal-dialog modal-md">
                <div class="modal-content">
                        <div class="modal-body" style="padding:0px;">
				<ul class="nav nav-tabs">
					<li class="active col-xs-6 text-center" style="padding:0px!important;">
						<a data-toggle="tab" href="#login_inside">
							<h4>Connexion</h4>
						</a>
					</li>
					<li class="text-center col-xs-6" style="padding:0px!important;">
						<a data-toggle="tab" href="#signup_inside">
							<h4>Inscritpion</h4>
						</a>
					</li>
				</ul>
				<div class="text-center tab-content" style="margin:25px;">
					<div id="login_inside" class="tab-pane active">
						<form method="post" action="login.php">
							<input type="hidden" name="login-form">
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
								<input type="text" class="form-control input-lg" name="user_login" placeholder="Nom d'utilisateur" required>
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
								<input type="password" class="form-control input-lg" name="user_password" placeholder="Mot de passe" requiured>
							</div>
							<div class="form-group">
								<br>
								<div class="text-center">
									<button type="submit" class="btn btn-primary btn-lg">Connexion</button>
								</div>
								<br>
							</div>
						</form>
					</div>
					<div id="signup_inside" class="tab-pane fade">
						<form class="form-horizontal" method="post" action="login.php">
							<div class="form-group row">
								<div class="col-xs-6">
									<input type="text" class="form-control input-md" placeholder="Prénom" name="user_firstname" required>
								</div>
								<div class="col-xs-6">
									<input type="text" class="form-control input-md" placeholder="Pseudonyme" name="user_login" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-6">
									<input type="text" class="form-control input-md" placeholder="Nom" name="user_lastname" required>
								</div>
								<div class="col-xs-6">
									<input type="password" class="form-control input-md" placeholder="Mot de passe" name="user_password" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-6">
									<input type="email" class="form-control input-md" placeholder="E-mail" name="user_mail" required>
								</div>
								<div class="col-xs-6">
									<input type="password" class="form-control input-md" placeholder="Mot de passe (confirm.)" name="user_password_confirm" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-6">
<?php
									include 'country.html';
?>
								</div>
								<div class="col-xs-6">
									<select name="user_sexe" class="selectpicker form-control" data-width="100" required>
										<option data-hidden="true">Sexe</option>
										<option value="homme">Homme</option>
										<option value="femme">Femme</option>
										<option value="autre">Autre</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="form-inline row">
									<div class="col-xs-offset-4 col-xs-4 col-md-offset-4 col-md-4">
										<label for="user_age">Âge : </label>
										<input class="form-control" style="width:67%;" id="user_age" type="number" min="3" max="99" value="17" name="user_age">
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-6 col-xs-offset-3 text-center">
									<button type="submit" class="btn btn-primary btn-lg">Enregistrer</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
