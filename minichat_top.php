<?php
include 'logout_modal.php';
if(!isset($_SESSION['free_user_nickname'])){
?>
	<nav class="navbar navbar-inverse slideInFromTop" style="margin-bottom:5%">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
					<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button>
				<span class="navbar-brand"><i class="fa fa-comments-o"></i></span>
			</div>
			<div class="collapse navbar-collapse" id="navigation">
				<ul class="nav navbar-nav">
					<li><a href="/">Accueil</a></li>
					<li><a href="member.php">Espace membres</a></li>
					<li class="active"><a href="minichat.php">Salle de chat</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a data-toggle="modal" data-target="#logout" style="cursor:pointer;">Déconnexion <span class="glyphicon glyphicon-log-out"></span></a></li>
				</ul>
			</div>
		</div>
	</nav>
<?php
} else {
	include 'login_modal.php';
?>
	<nav class="navbar navbar-inverse slideInFromTop" style="margin-bottom:5%;">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
					<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button>
				<span class="navbar-brand"><i class="fa fa-comments-o"></i></span>
			</div>
			<div class="collapse navbar-collapse" id="navigation">
				<ul class="nav navbar-nav">
					<li><a href="/">Accueil</a></li>
					<li class="active"><a href="minichat.php">Salle de chat</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Session libre : <?php echo $_SESSION['free_user_nickname']; ?><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a data-toggle="modal" data-target="#login" style="cursor:pointer;">Connexion</a></li>
							<li role="separator" class="divider"></li>
							<li><a data-toggle="modal" data-target="#logout" style="cursor:pointer;">Quitter</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
<?php
}
?>
