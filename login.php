<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
	<head>
		<title>LWS Minichat</title>
		<meta charset="utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="/js/loader.js"></script>
<?php
                if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
                        <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
<?php
                } else {
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
                        <link rel="stylesheet" href="css/paper.css">
<?php
                }
?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	</head>
	<body>
		<div class="loader"></div>
<?php
			if(isset($_POST['free'])){
				$free_user_query=mysqli_query($connexion,"select * from users where user_is_free=TRUE");
				while($free_user=mysqli_fetch_array($free_user_query)){
					if(strtolower($free_user['user_nickname'])==strtolower(Addslashes(htmlspecialchars($_POST['user_nickname'])))){
						if(date("d-m-Y",strtotime("now"))!=date("d-m-Y",strtotime($free_user['user_logged_since']))){
							if(mysqli_query($connexion,"delete from posted_by where user_id=".$free_user['user_id'])===TRUE){
								if(mysqli_query($connexion,"delete from users where user_id=".$free_user['user_id'])!==TRUE){
									$error="Damn, erreur #3 lors de la libération du pseudo '".$free_user['user_nickname']."' (".mysqli_error($connexion).")";
								}
							} else {
								$error="Damn, erreur SQL #2 (".mysqli_error($connexion).")";
							}
						} else {
							$error="Le pseudo '".Addslashes(htmlspecialchars($_POST['user_nickname']))."' est déjà utilisé !<br><br><small>Astuce : Réessayez demain !</small>";
						}
					}
				}
				$max_uid_query=mysqli_query($connexion,"select max(user_id) from users");
				$max_uid=mysqli_fetch_array($max_uid_query);
                                if((!isset($error)) and (mysqli_query($connexion,"insert into users values (".($max_uid[0]+1).",TRUE,'".Addslashes(htmlspecialchars($_POST['user_nickname']))."','','','','','".date('Y-m-d')."','','','',0,TRUE,'user_image/new.png','')")===TRUE)){
					#Destruction de la session précédente
					$_SESSION = array();
					session_destroy();
					unset($_SESSION);
					#Création de la nouvelle session
					session_start();
					$_SESSION['user_id']=$max_uid[0]+1;
					$_SESSION['free_user_nickname']=Addslashes(htmlspecialchars($_POST['user_nickname']));
					header("Location: chatroom.php");
					die();
				} else {
					if(!isset($error)){ $error="Une erreur (1) est survenue lors de la création de la session libre pour '".Addslashes(htmlspecialchars($_POST['user_nickname']))."' (".mysqli_error($connexion).") !"; }
				}
				if(isset($error)){
?>
                                        <div class="jumbotron text-center">
                                                <h1>LWS Minichat<br><small>Local Web Server</small></h1>
                                        </div>
                        <div class="container" style="padding: 5%;">
                                        <div class="alert alert-danger text-center">
                                                <h2>Erreur !</h2><br><h4><?php echo $error; ?></h4><br>
                                                <a href="/">
                                                        <button type="button" class="btn btn-default" autofocus>Retour</button>
                                                </a>
                                        </div>
<?php
				}
			} elseif(isset($_POST['login-form'])){
				$user_password_query="select UNHEX(REVERSE(UNHEX(user_password))) from users where user_login like '".Addslashes(htmlspecialchars($_POST['user_login']))."'";
				$user_password_result=mysqli_query($connexion,$user_password_query) or die ("Erreur de requête : ".mysqli_error($connexion));
				$user_password=mysqli_fetch_array($user_password_result);
				$user_data=mysqli_fetch_array(mysqli_query($connexion,"select * from users where user_login like '".Addslashes(htmlspecialchars($_POST['user_login']))."'"));
				if(($user_password[0]==Addslashes(htmlspecialchars($_POST['user_password']))) and (Addslashes(htmlspecialchars($_POST['user_password']))!="")){
					if($user_data['user_checked']){
						#Destruction de la session précédente
						$_SESSION = array();
						session_destroy();
						unset($_SESSION);
						#Création de la nouvelle session
						session_start();
						$_SESSION["user_id"] = $user_data["user_id"];
						$_SESSION["user_nickname"] = $user_data["user_nickname"];
						$_SESSION["user_lastname"] = $user_data["user_lastname"];
						$_SESSION["user_firstname"] = $user_data["user_firstname"];
						$_SESSION["user_login"] = $user_data["user_login"];
						$_SESSION["user_mail"] = $user_data["user_mail"];
						$_SESSION["user_logged_since"] = $user_data["user_logged_since"];
						$_SESSION["user_country"] = $user_data["user_country"];
						$_SESSION["user_sexe"] = $user_data["user_sexe"];
						$_SESSION["user_age"] = $user_data["user_age"];
						if(empty($user_data['user_image'])){ $_SESSION['user_image']="user_image/new.png"; } else { if(file_exists($user_data['user_image'])){ $_SESSION['user_image']=$user_data['user_image']; } else { $_SESSION['user_image']="user_image/new.png"; } }
						if(empty($user_data['user_theme'])){ $_SESSION['user_theme']="paper"; } else { $_SESSION['user_theme']=$user_data['user_theme']; }
?>
                				<div class="jumbotron text-center">
                       	 			<h1>LWS Minichat<br><small>Local Web Server</small></h1>
                				</div>
                	        <div class="container" style="padding: 5%;">
						<div class="alert alert-success text-center">
							<h2>Authentification réussie !</h2><br><h4>Bienvenue, <?php echo ucfirst($user_data['user_firstname']); ?>.</h4><br>
							<a href="/">
								<button type="button" class="btn btn-default" autofocus>Poursuivre</button>
							</a>
						</div>
<?php
					} else {
?>
						<div class="jumbotron text-center">
							<h1>LWS Minichat<br><small>Local Web Server</small></h1>
						</div>
						<div class="container">
							<div class="alert alert-danger text-center">
								<h2>Oups...</h2><h4>Vous devez valider votre compte avant de vous connecter !<br>Vous devriez avoir reçu un mail à l'adresse '<?php echo $user_data['user_mail']; ?>', si ce n'est pas le cas, veuillez contacter <a href='mailto:raph.berteaud@gmail.com'>l'administrateur</a>.</h4>
								<a href="/">
									<button type="button" class="btn btn-default" autofocus>Retour</button>
								</a>
							</div>
<?php
					}
				} else {
?>
					<div class="jumbotron text-center">
						<h1>LWS Minichat<br><small>Local Web Server</small></h1>
					</div>
					<div class="container" style="padding: 5%;">
						<div class="alert alert-danger text-center">
							<h2>Erreur d'authentification !</h2><br><h4>Vous avez saisi une mauvaise combinaison 'Nom d'utilisateur/Mot de passe'.<br>Veuillez réessayer.</h4>
							<a href="/">
								<button type="button" class="btn btn-default" autofocus>Retour</button>
							</a>
						</div>
<?php
				}
			} else {
				if(Addslashes(htmlspecialchars($_POST['user_password']))!=Addslashes(htmlspecialchars($_POST['user_password_confirm']))){
					$error="Vous n'avez pas saisi les mêmes mots de passe !";
				}
				$check_login_query=mysqli_query($connexion,"select * from users where user_login like '".Addslashes(htmlspecialchars($_POST['user_login']))."'") or die ("Erreur SQL : ".mysqli_error($connexion));
				if(mysqli_num_rows($check_login_query)>0){
					$error="Le nom d'utilisateur '".Addslashes(htmlspecialchars($_POST['user_login']))."' est déjà pris !";
				}
				$check_mail_query=mysqli_query($connexion,"select * from users where user_mail like '".Addslashes(htmlspecialchars($_POST['user_mail']))."'") or die ("Erreur SQL : ".mysqli_error($connexion));
				if(mysqli_num_rows($check_mail_query)>0){
					$error="L'adresse mail '".Addslashes(htmlspecialchars($_POST['user_mail']))."' est déjà prise !";
				}
				if($_POST['user_country']=="Pays"){
					$error="Vous devez indiquer votre pays !";
				}
				if($_POST['user_sexe']=="Sexe"){
					$error="Vous devez indiquer votre sexe !";
				}
				if(!isset($error)){
					$max_uid_query="select max(user_id) from users";
					$max_uid_result=mysqli_query($connexion,$max_uid_query);
					$max_uid=mysqli_fetch_array($max_uid_result);
					if(mysqli_query($connexion,"insert into users values (".($max_uid[0]+1).",FALSE,'".Addslashes(htmlspecialchars($_POST['user_login']))."','".strtolower(Addslashes(htmlspecialchars($_POST['user_lastname'])))."','".strtolower(Addslashes(htmlspecialchars($_POST['user_firstname'])))."','".Addslashes(htmlspecialchars($_POST['user_login']))."',HEX(REVERSE(HEX('".Addslashes(htmlspecialchars($_POST['user_password']))."'))),'".date("Y-m-d")."','".strtolower(Addslashes(htmlspecialchars($_POST['user_mail'])))."','".$_POST['user_country']."','".$_POST['user_sexe']."',".$_POST['user_age'].",FALSE,'user_image/new.png','paper')")===TRUE){
						#Destruction de la session précédente
						$_SESSION = array();
						session_destroy();
						unset($_SESSION);
						#Création de la nouvelle session
						session_start();
						$user_data=mysqli_fetch_array(mysqli_query($connexion,"select * from users where user_id=".($max_uid[0]+1)));
						$uid=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex(convert(".$user_data['user_id'].",char))))"))[0];
						$tid=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('".$user_data['user_logged_since']."')))"))[0];
						$mid=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('".$user_data['user_mail']."')))"))[0];
						$req=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('check')))"))[0];

						$mail = $user_data['user_mail'];
						if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
							$crlf = "\r\n";
						} else {
							$crlf = "\n";
						}

						$message_txt = "Bienvenue sur Lewis,".$crlf.$crlf;
						$message_txt.="Pour activer votre compte, veuillez cliquer sur le lien ci-dessous, ".$crlf;
						$message_txt.="ou copier/coller dans votre navigateur internet.".$crlf.$crlf;
						$message_txt.="http://lewis.servehttp.com/verification.php?uid=".$uid."&tid=".$tid."&mid=".$mid."&req=".$req;
						$message_txt.=$crlf.$crlf.$crlf."---------------".$crlf.$crlf;
						$message_txt.="Ceci est un mail automatique, merci de ne pas y répondre.";

						$message_html="<html>".$crlf;
						$message_html.="<body style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-weight: lighter;\">".$crlf;
						$message_html.="<div style=\"margin: auto; width:95%; padding:20px; text-align: center; background-color:#eee;\">".$crlf;
						$message_html.="<h1 style=\"font-weight: lighter; font-size: 50px;\">LWS Minichat<br><span style=\"font-size:65%; color: #777;\">Local Web Server</span></h1>".$crlf;
						$message_html.="</div>".$crlf;
						$message_html.="<div style=\"margin: auto; width:80%;\">".$crlf;
						$message_html.="<h4>Bienvenue sur Lewis,</h4>".$crlf;
						$message_html.="<p>Pour activer votre compte, veuillez cliquer sur le lien ci-dessous,<br>ou le copier/coller dans votre navigateur Internet.</p>".$crlf;
						$message_html.="<a href=\"http://lewis.servehttp.com/verification.php?uid=".$uid."&tid=".$tid."&mid=".$mid."&req=".$req."\"><button type=\"button\" style=\"padding:10px 16px; border-radius: 5px; background-color: #50b4d5; border: 1px solid #209cc0; color: white; font-size: 17px; cursor: pointer;\">Valider compte</button></a>".$crlf;
						$message_html.="<p style=\"color: #777; font-size: 80%; position: absolute; bottom: 5px; right: 5px;\">Ceci est un mail automatique, merci de ne pas y répondre.</p>".$crlf;
						$message_html.="</div>".$crlf;
						$message_html.="</body>".$crlf;
						$message_html.="</html>";

						$boundary = "-----=".md5(rand());

						$sujet = "Inscription sur LWS";

						$header = "From: \"LWS Minichat\" <raph.berteaud@gmail.com>".$crlf;
						$header.= "Reply-to: \"LWS Minichat\" <raph.berteaud@gmail.com>".$crlf;
						$header.= "MIME-Version: 1.0".$crlf;
						$header.= "Content-Type: multipart/alternative;".$crlf." boundary=\"$boundary\"".$crlf;

						$message = $crlf."--".$boundary.$crlf;

						$message.= "Content-Type: text/plain; charset=\"utf8\"".$crlf;
						$message.= "Content-Transfer-Encoding: 8bit".$crlf;
						$message.= $crlf.$message_txt.$crlf;

						$message.= $crlf."--".$boundary.$crlf;

						$message.= "Content-Type: text/html; charset=\"utf8\"".$crlf;
						$message.= "Content-Transfer-Encoding: 8bit".$crlf;
						$message.= $crlf.$message_html.$crlf;

						$message.= $crlf."--".$boundary."--".$crlf;
						$message.= $crlf."--".$boundary."--".$crlf;

						if(mail($mail,$sujet,$message,$header)===TRUE){
?>
							<div class="jumbotron text-center">
								<h1>LWS Minichat<br><small>Local Web Server</small></h1>
							</div>
							<div class="container">
								<div class="alert alert-success text-center">
									<h2>Enregistrement réussi !</h2><br><h4>Un mail vous a été envoyé à l'adresse '<?php echo $user_data['user_mail']; ?>' afin de valider votre compte.</h4>
									<a href="/">
										<button type="button" class="btn btn-default" autofocus>Retour</button>
									</a>
								</div>
<?php
						} else {
?>
							<div class="jumbotron text-center">
								<h1>LWS Minichat<br><small>Local Web Server</small></h1>
							</div>
							<div class="container">
								<div class="alert alert-danger text-center">
									<h2>Oups...</h2><h4>Une erreur s'est produite lors de l'envoi du mail à l'adresse '<?php echo $user_data['user_mail']; ?>'.<br>Veuillez contacter <a href='mailto:raph.berteaud@gmail.com'>l'administrateur</a>.</h4>
									<a href="/">
										<button type="button" class="btn btn-default" autofocus>Retour</button>
									</a>
								</div>
<?php
						}
					} else {
						$error="Damn, erreur SQL #1 (".mysqli_error($connexion).")";
					}
					if(isset($error)){
?>
	                			<div class="jumbotron text-center">
        	                			<h1>LWS Minichat<br><small>Local Web Server</small></h1>
        	        			</div>
                			        <div class="container" style="padding: 5%;">
							<div class="alert alert-danger text-center">
								<h2>Oups...</h2><br><h4><?php echo $error; ?></h4><br>
								<a href="/">
									<button type="button" class="btn btn-default" autofocus>Retour</button>
								</a>
<?php
					}
				} else {
?>
                			<div class="jumbotron text-center">
                        			<h1>LWS Minichat<br><small>Local Web Server</small></h1>
                			</div>
                        <div class="container" style="padding: 5%;">
					<div class="alert alert-danger text-center">
						<h2>Erreur d'enregistrement !</h2><br><h4><?php echo $error; ?></h4><br>
						<a href="/">
							<button type="button" class="btn btn-default" autofocus>Retour</button>
						</a>
					</div>
<?php
				}
			}
?>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
	</body>
</html>
