<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
<head>
  <title>LWS Minichat</title>
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="/js/loader.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php
  if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
    ?>
    <link rel="stylesheet" href="https://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
    <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
    <?php
  } else {
    ?>
    <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
    <link rel="stylesheet" href="css/paper.css">
    <?php
  }
  ?>
  <link rel='stylesheet' type='text/css' href='css/slideIn.php' />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
  <link rel="icon" type="image/png" href="img/favicon.png" />
  <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /><![endif]-->
</head>
<body>
  <div class="loader"></div>
<?php
  if(isset($_SESSION['user_id'])){
    include 'logout_modal.php';
    if(!isset($_SESSION['free_user_nickname'])){
?>
      <nav class="navbar navbar-inverse slideInFromTop" style="margin-bottom:0px;">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
              <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <span class="navbar-brand"><i class="fa fa-comments-o" style="text-shadow: 0 0 5px #eee; font-size: 48px; color: #fff; height: 72px; width: 72px; line-height: 50%; text-align: center;"></i></span>
          </div>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
              <li class="active"><a href="/">Accueil</a></li>
              <li><a href="member.php">Espace membres</a></li>
              <li><a href="minichat.php">Salle de chat</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a data-toggle="modal" data-target="#logout" style="cursor:pointer;">Déconnexion <span class="glyphicon glyphicon-log-out"></span></a></li>
            </ul>
          </div>
        </div>
      </nav>
<?php
    } else {
      include 'login_modal.php';
?>
      <nav class="navbar navbar-inverse slideInFromTop" style="margin-bottom:0px;">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
              <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <span class="navbar-brand"><i class="fa fa-comments-o" style="text-shadow: 0 0 5px #eee; font-size: 48px; color: #fff; height: 72px; width: 72px; line-height: 50%; text-align: center;"></i></span>
          </div>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
              <li class="active"><a href="/">Accueil</a></li>
              <li><a href="minichat.php">Salle de chat</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Session libre : <?php echo $_SESSION['free_user_nickname']; ?><span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a data-toggle="modal" data-target="#login" style="cursor:pointer;">Connexion</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a data-toggle="modal" data-target="#logout" style="cursor:pointer;">Quitter</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
<?php
    }
?>
    <div class="jumbotron text-center no-sm">
      <h1>LWS Minichat<br><small>Local Web Server</small></h1>
    </div>
    <div class="container">
      <div class="panel panel-default col-md-6 col-md-offset-3 col-xs-12 slideInFromBottom" style="padding:0px!important;">
        <div class="panel-heading text-center">
          <h3>Chatroom</h3>
        </div>
        <div class="panel-body text-center">
          <form method="post" action="minichat.php">
            <input type="hidden" name="chatroom-form">
            <h4>Choisissez votre salle de chat, <?php if(isset($_SESSION['free_user_nickname'])){echo $_SESSION['free_user_nickname'];} else {echo $_SESSION['user_login'];}?> :</h4>
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                <input type="text" class="form-control input-lg" name="chatroom_name" placeholder="Chatroom (ex : lolcat)" autofocus required>
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary btn-lg">Entrer</button>
                </span>
              </div>
              <div class="input-group">
                <div class="checkbox" <?php if(isset($_SESSION['free_user_nickname'])){ echo "disabled"; } ?>>
                  <label<?php if(isset($_SESSION['free_user_nickname'])){ echo " style='cursor:not-allowed;'"; } ?>><input type="checkbox" name="chatroom_is_private" id="toggle" value="true" <?php if(isset($_SESSION['free_user_nickname'])){ echo "disabled"; } ?>>Privée</label>
                  </div>
                </div>
                <div id="toggled" style="display:none;">
                  <div class="input-group">
                    <input type="password" class="form-control" id="chatroom_password" placeholder="Mot de passe" name="chatroom_password" maxlength="63" <?php if(isset($_SESSION['free_user_nickname'])){ echo "disabled"; } ?>>
                  </div>
                </div>
              </form>
            </div>
            <div class="panel-footer text-center">
<?php
              echo "Salles ouvertes :";
              $query_or=mysqli_query($connexion,"select * from chatroom where chatroom_is_private=FALSE order by RAND() limit 7") or die ("Erreur SQL : ".$mysqli_error($connexion));
              while($open_rooms=mysqli_fetch_array($query_or)){
                echo " '<b>".$open_rooms['chatroom_name']."</b>'";
              }
              echo " ...";
?>
            </div>
          </div>
        </div>
<?php
      } else {
        header("Location: index.php");
      }
?>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
      <script src="/js/chk-toggle.js"></script>
    </body>
</html>
