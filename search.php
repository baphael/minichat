<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
        <head>
                <title>LWS Minichat</title>
                <meta charset="utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script src="/js/loader.js"></script>
<?php
                if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
                        <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
<?php
                } else {
?>
                        <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
                        <link rel="stylesheet" href="css/paper.css">
<?php
                }
?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
		<link rel="icon" type="image/png" href="img/favicon.png" />
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        </head>
        <body>
		<div class="loader"></div>
		<nav class="navbar navbar-inverse" style="margin-bottom:0px;">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
						<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
					<span class="navbar-brand"><i class="fa fa-comments-o" style="text-shadow: 0 0 5px #eee; font-size: 48px; color: #fff; height: 72px; width: 72px; line-height: 50%; text-align: center;"></i></span>
				</div>
				<div class="collapse navbar-collapse" id="navigation">
					<ul class="nav navbar-nav">
						<li><a href="/">Accueil</a></li>
						<li class="active"><a href="member.php">Espace membres</a></li>
						<li><a href="minichat.php">Salle de chat</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a data-toggle="modal" data-target="#logout" style="cursor:pointer;">Déconnexion <span class="glyphicon glyphicon-log-out"></span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="jumbotron text-center">
			<h1>LWS Minichat<br><small>Local Web Server</small></h1>
		</div>
		<div class="container">
			<form action='<?php echo $_SERVER['PHP_SELF']; ?>' class="row" method="get">
				<div class="col-xs-12 col-md-offset-3 col-md-6 form-group">
					<div class="input-group col-xs-12">
						<input type="text" maxlength="31" <?php if(isset($_GET['user'])){ echo "value='".$_GET['user']."'"; } ?> name="user" class="form-control input-lg" placeholder="Rechercher utilisateur" required>
						<span class="input-group-btn">
							<button class="btn btn-primary" style="padding:15px;" type="submit">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</span>
					</div>
				</div>
			</form>
			<div class="row">
<?php
				if(isset($_SESSION['user_id'])){
					if(isset($_GET['user'])){
						$users_like=mysqli_query($connexion,"select * from users where (user_nickname like '%".$_GET['user']."%' and user_is_free=FALSE)");
						if(mysqli_num_rows($users_like)>0){
							echo "<div class='text-center alert alert-success col-xs-12 col-md-offset-4 col-md-4'><b>".mysqli_num_rows($users_like)."</b> résultat(s) correspondant(s)</div><div class='clearfix'></div><div class='table-responsive'><table class='table table-hover'><thead><tr><th>Prénom</th><th>Nom</th><th>Pseudo.</th><th>Adresse mail</th><th>Détails</th></tr></thead><tbody>";
							while($uld=mysqli_fetch_array($users_like)){
								echo "<tr><td>".ucfirst($uld['user_firstname'])."</td><td>".ucfirst($uld['user_lastname'])."</td><td>".$uld['user_nickname']."</td><td><a href='mailto:".$uld['user_mail']."'>".$uld['user_mail']."</a></td><td><a href='member.php?id=".$uld['user_id']."'>voir profil</a></tr>";
							}
							echo "</tbody></table></div>";
						} else {
							echo "<div class='text-center alert alert-warning col-xs-12 col-md-offset-4 col-md-4'>Aucun résultat ne correspond au critère de recherche '<b>".$_GET['user']."</b>'</div>";
						}
					} else {
						header("Location: member.php");
					}
				} else {
					header("Location: index.php");
				}
?>
			</div>
		</div>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
	</body>
</html>
