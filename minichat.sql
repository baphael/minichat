DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `is_user_free`;
DROP TABLE IF EXISTS `message`;
DROP TABLE IF EXISTS `chatroom`;
DROP TABLE IF EXISTS `posted_in`;
DROP TABLE IF EXISTS `posted_by`;
DROP TABLE IF EXISTS `user_likes`;

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `user_is_free` boolean NOT NULL,
  `user_nickname` varchar(64) NOT NULL,
  `user_lastname` varchar(64) NOT NULL,
  `user_firstname` varchar(64) NOT NULL,
  `user_login` varchar(64) NOT NULL,
  `user_password` varchar(1000) NOT NULL,
  `user_logged_since` date NOT NULL,
  `user_mail` varchar(128) NOT NULL,
  `user_country` varchar(128) NOT NULL,
  `user_sexe` varchar(32) NOT NULL,
  `user_age` int NOT NULL,
  `user_checked` boolean NOT NULL,
  `user_image` varchar(255),
  `user_theme` varchar(128),
  PRIMARY KEY (`user_id`)
);

CREATE TABLE `message` (
        `message_id` int NOT NULL,
        `message_content` blob(4000) NOT NULL,
	`message_time` datetime NOT NULL,
        PRIMARY KEY (`message_id`)
);

CREATE TABLE `chatroom` (
        `chatroom_id` int NOT NULL,
        `chatroom_name` varchar(128),
	`chatroom_is_private` boolean NOT NULL,
	`chatroom_password` varchar(1000),
	PRIMARY KEY (`chatroom_id`)
);

CREATE TABLE `posted_in` (
        `message_id` int NOT NULL,
        `chatroom_id` int NOT NULL,
        CONSTRAINT `fkmpi` FOREIGN KEY (message_id) references message(message_id),
        CONSTRAINT `fkcpi` FOREIGN KEY (chatroom_id) references chatroom(chatroom_id)
);

CREATE TABLE `posted_by` (
        `message_id` int NOT NULL,
        `user_id` int NOT NULL,
        CONSTRAINT `fkmpb` FOREIGN KEY (message_id) references message(message_id),
        CONSTRAINT `fkupb` FOREIGN KEY (user_id) references users(user_id)
);

CREATE TABLE `user_likes` (
	`user_id_from` int NOT NULL,
	`user_id_to` int NOT NULL,
	CONSTRAINT `fkulf` FOREIGN KEY (user_id_from) references users(user_id),
	CONSTRAINT `fkult` FOREIGN KEY (user_id_to) references users(user_id)
);
