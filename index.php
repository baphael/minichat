<?php
session_cache_limiter('private_no_expire, must-revalidate');
session_start();
$connexion=mysqli_connect("localhost","minichat","minichatonlewis","minichat") or die ("Impossible de se connecter à la base de données ! (".mysqli_error($connexion).")");
?>
<html>
<head>
  <title>LWS Minichat</title>
  <meta charset="utf-8">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="/js/loader.js"></script>
  <?php
  if((isset($_SESSION['user_theme'])) and (!empty($_SESSION['user_theme']))){
    ?>
    <link rel="stylesheet" href="https://bootswatch.com/3/<?php echo $_SESSION['user_theme']; ?>/bootstrap.min.css">
    <link rel="stylesheet" href="css/<?php echo $_SESSION['user_theme']; ?>.css">
    <?php
  } else {
    ?>
    <link rel="stylesheet" href="http://bootswatch.com/3/paper/bootstrap.min.css">
    <link rel="stylesheet" href="css/paper.css">
    <?php
  }
  ?>
  <link rel='stylesheet' type='text/css' href='css/slideIn.php' />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css">
  <link rel="icon" type="image/png" href="img/favicon.png" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
  <div class="loader"></div>
  <?php 		include 'forgot_modal.php'; ?>
  <div class="jumbotron text-center">
    <h1>LWS Minichat<small><br>Local Web Server</small></h1>
  </div>
  <div class="container">
    <?php
    if(isset($_GET['mail_for_password'])){
      if(!empty($_GET['mail_for_password'])){
        if(mysqli_num_rows(mysqli_query($connexion,"select * from users where user_mail like '".Addslashes(htmlspecialchars($_GET['mail_for_password']))."'"))>0){
          $grab=mysqli_fetch_array(mysqli_query($connexion,"select * from users where user_mail like '".Addslashes(htmlspecialchars($_GET['mail_for_password']))."'"));
          $uid=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex(convert(".$grab['user_id'].",char))))"))[0];
          $tid=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('".$grab['user_logged_since']."')))"))[0];
          $mid=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('".$grab['user_mail']."')))"))[0];
          $req=mysqli_fetch_array(mysqli_query($connexion,"select hex(reverse(hex('password')))"))[0];

          $mail = $grab['user_mail'];
          if (!preg_match("#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#", $mail)){
            $crlf = "\r\n";
          } else {
            $crlf = "\n";
          }

          $message_txt = "Bonjour ".ucfirst($grab['user_firstname']).",".$crlf.$crlf;
          $message_txt.="Vous reçevez ce mail car vous avez récemment effectué une demande de renouvellement du mot de passe de votre compte sur LWS.".$crlf;
          $message_txt.="Pour effectuer ce renouvellement, veuillez cliquer sur le lien ci-dessous,".$crlf;
          $message_txt.="ou le copier/coller dans votre navigateur :".$crlf.$crlf;
          $message_txt.="http://minichat.lws.ovh/verification.php?uid=".$uid."&tid=".$tid."&mid=".$mid."&req=".$req;
          $message_txt.=$crlf.$crlf."Si vous n'avez pas effectué cette demande de renouvellement de mot de passe, veuillez ignorer ce mail.".$crlf;
          $message_txt.=$crlf.$crlf."---------------".$crlf.$crlf;
          $message_txt.="Ceci est un mail automatique, merci de ne pas y répondre.";

          $message_html="<html>".$crlf;
          $message_html.="<body style=\"font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-weight: lighter;\">".$crlf;
          $message_html.="<div style=\"margin: none; width:95%; padding:20px; text-align: center; background-color:#eee;\">".$crlf;
          $message_html.="<h1 style=\"font-weight: lighter; font-size: 50px;\">LWS Minichat<br><span style=\"font-size:65%; color: #777;\">Local Web Server</span></h1>".$crlf;
          $message_html.="</div>".$crlf;
          $message_html.="<div style=\"margin: auto; width:80%;\">".$crlf;
          $message_html.="<h4>Bonjour ".ucfirst($grab['user_firstname']).",</h4>".$crlf;
          $message_html.="<p>Vous reçevez ce mail car vous avez récemment effectué une demande de renouvellement du mot de passe de votre compte sur LWS.<br>Pour effectuer ce renouvellement, veuillez cliquer sur le lien ci-dessous :</p>".$crlf;
          $message_html.="<a href=\"http://minichat.lws.ovh/verification.php?uid=".$uid."&tid=".$tid."&mid=".$mid."&req=".$req."\"><button type=\"button\" style=\"padding:10px 16px; border-radius: 5px; background-color: #50b4d5; border: 1px solid #209cc0; color: white; font-size: 17px; cursor: pointer;\">Renouveler mot de passe</button></a>".$crlf;
          $message_html.="<p style=\"color: #777; font-size: 80%; position: absolute; bottom: 5px; right: 5px;\">Si vous n'êtes pas à l'origine de cette demande de renouvellement de mot de passe, veuillez ignorer ce mail.<br><br>Ceci est un mail automatique, merci de ne pas y répondre.</p>".$crlf;
          $message_html.="</div>".$crlf;
          $message_html.="</body>".$crlf;
          $message_html.="</html>";

          $boundary = "-----=".md5(rand());
          $sujet = "LWS - Renouvellement mot de passe";

          $header = "From: \"LWS Minichat\" <raph.berteaud@gmail.com>".$crlf;
          $header.= "Reply-to: \"LWS Minichat\" <raph.berteaud@gmail.com>".$crlf;
          $header.= "MIME-Version: 1.0".$crlf;
          $header.= "Content-Type: multipart/alternative;".$crlf." boundary=\"$boundary\"".$crlf;

          $message = $crlf."--".$boundary.$crlf;

          $message.= "Content-Type: text/plain; charset=\"utf8\"".$crlf;
          $message.= "Content-Transfer-Encoding: 8bit".$crlf;
          $message.= $crlf.$message_txt.$crlf;

          $message.= $crlf."--".$boundary.$crlf;

          $message.= "Content-Type: text/html; charset=\"utf8\"".$crlf;
          $message.= "Content-Transfer-Encoding: 8bit".$crlf;
          $message.= $crlf.$message_html.$crlf;

          $message.= $crlf."--".$boundary."--".$crlf;
          $message.= $crlf."--".$boundary."--".$crlf;

          if(mail($mail,$sujet,$message,$header)===TRUE){
            ?>
            <div class="alert alert-success alert-dismissible text-center" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Un mail vous a été envoyé à l'adresse '<?php echo $grab['user_mail']; ?>', vous indiquant la démarche à suivre pour renouveler votre mot de passe.
            </div>
            <?php
          } else {
            ?>
            <div class="alert alert-danger alert-dismissible text-center" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Une erreur s'est produite lors de l'envoi du mail à l'adresse '<?php echo $grab['user_mail']; ?>'.<br>Veuillez contacter <a href='mailto:raph.berteaud@gmail.com'>l'administrateur</a>.
            </div>
            <?php
          }
        } else {
          ?>
          <div class="alert alert-danger alert-dismissible text-center" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            L'adresse mail '<?php echo Addslashes(htmlspecialchars($_GET['mail_for_password'])); ?>' ne correspond à aucun compte Lewis valide.
          </div>
          <?php
        }
      } else {
        ?>
        <div class="alert alert-danger alert-dismissible text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          Vous avez renseigné une adresse vide !
        </div>
        <?php
      }
    }
    if(!isset($_SESSION['user_id'])){
      ?>
      <div class="panel panel-default col-md-6 col-md-offset-3 col-xs-12 slideInFromBottom" style="padding: 0px!important;">
        <div class="panel-heading text-center" style="padding:0px!important;">
          <ul class="nav nav-tabs">
            <li class="active col-xs-4 text-center" style="padding: 0px!important;">
              <a data-toggle="tab" href="#freeuse">
                <h4>Session</h4>
              </a>
            </li>
            <li class="col-xs-4 text-center" style="padding:0px!important;">
              <a data-toggle="tab" href="#login">
                <h4>Connexion</h4>
              </a>
            </li>
            <li class="text-center col-xs-4" style="padding:0px!important;">
              <a data-toggle="tab" href="#signup">
                <h4>Inscritpion</h4>
              </a>
            </li>
          </ul>
        </div>
        <div class="panel-body text-center tab-content">
          <div id="freeuse" class="tab-pane fade in active">
            <form method="post" action="login.php">
              <input type="hidden" name="free">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" class="form-control input-lg" name="user_nickname" placeholder="Pseudonyme" autofocus required>
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary" style="padding: 15px;"><i class="glyphicon glyphicon-arrow-right"></i></button>
                </span>
              </div>
            </form>
          </div>
          <div id="login" class="tab-pane fade">
            <form method="post" action="login.php">
              <input type="hidden" name="login-form">
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input type="text" class="form-control input-lg" name="user_login" placeholder="Nom d'utilisateur" required>
              </div>
              <br>
              <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input type="password" class="form-control input-lg" name="user_password" placeholder="Mot de passe" requiured>
              </div>
              <div class="form-group"><br>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary btn-lg">Connexion</button>
                </div>
                <br>
              </div>
            </form>
          </div>
          <div id="signup" class="tab-pane fade">
            <form class="form-horizontal" method="post" action="login.php">
              <div class="form-group row">
                <div class="col-xs-6">
                  <input type="text" class="form-control input-md" placeholder="Prénom" name="user_firstname" required>
                </div>
                <div class="col-xs-6">
                  <input type="text" class="form-control input-md" placeholder="Pseudonyme" name="user_login" required>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-6">
                  <input type="text" class="form-control input-md" placeholder="Nom" name="user_lastname" required>
                </div>
                <div class="col-xs-6">
                  <input type="password" class="form-control input-md" placeholder="Mot de passe" name="user_password" required>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-xs-6">
                  <input type="email" class="form-control input-md" placeholder="E-mail" name="user_mail" required>
                </div>
                <div class="col-xs-6">
                  <input type="password" class="form-control input-md" placeholder="Mot de passe (confirm.)" name="user_password_confirm" required>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-xs-6">
                  <?php
                  include 'country.html';
                  ?>
                </div>
                <div class="col-xs-6">
                  <select name="user_sexe" class="selectpicker form-control" data-width="100%" required>
                    <option data-hidden="true">Sexe</option>
                    <option value="homme">Homme</option>
                    <option value="femme">Femme</option>
                    <option value="autre">Autre</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <div class="form-inline row">
                  <div class="col-xs-offset-4 col-xs-4 col-md-offset-4 col-md-4">
                    <label for="user_age">Âge : </label>
                    <input class="form-control" style="width:67%;" id="user_age" type="number" min="3" max="99" value="17" name="user_age">
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-xs-6 col-xs-offset-3 text-center">
                  <button type="submit" class="btn btn-primary btn-lg">Enregistrer</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="panel-footer text-center">
          <a class="text-center" data-toggle="modal" data-target="#forgot" style="cursor:pointer;">Mot de passe oublié ?</a>
        </div>
      </div>
      <?php
    } else {
      header('Location: chatroom.php');
    }
    ?>
  </div>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
</body>
</html>
